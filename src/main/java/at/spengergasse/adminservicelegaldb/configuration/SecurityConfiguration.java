package at.spengergasse.adminservicelegaldb.configuration;


import at.spengergasse.adminservicelegaldb.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Matthias Walk
 * @author Clemens Kern
 */

@EnableWebSecurity
@Configuration
@ComponentScan
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        //for testing 403 and 500 pages
        //auth.inMemoryAuthentication().withUser("admin1").password(encoder().encode("cisco")).roles("ADMIN");
        auth.authenticationProvider(authenticationProvider());
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    /*
    @Bean
    AuthenticationProvider adAuthenticationProvider(){
        ActiveDirectoryLdapAuthenticationProvider authenticationProvider =
                new ActiveDirectoryLdapAuthenticationProvider("<testDomain>", "<url>");

    }
    */




    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.userDetailsService(userDetailsService());
        httpSecurity
                .authorizeRequests()
                .anyRequest().permitAll();
        //Disables cross site scripting
    }


    @Override
    protected UserDetailsService userDetailsService() {
        return userDetailsService;
    }

}
