package at.spengergasse.adminservicelegaldb;

import at.spengergasse.adminservicelegaldb.models.entities.*;
import at.spengergasse.adminservicelegaldb.models.repositories.*;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * @author Matthias Walk
 */

@SuppressWarnings("ALL")
@RestController
public class InsertController {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userRepo;
    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    PrivilegeRepository privilegeRepository;
    @Autowired
    TagTypeRepository tagTypeRepository;
    @Autowired
    DataSource dataSource;
    @Autowired
    FolderRepository folderRepository;
    @Autowired
    DocumentTypeRepository documentTypeRepository;
    @Autowired
    CompanyContactRepository companyContactRepository;
    @Autowired
    CompanyFunctionRepository companyFunctionRepository;
    @Autowired
    LegalRepresentationRepository legalRepresentationRepository;

    @GetMapping("/insert")
    public String insert() throws Exception {
        System.out.println("Insert Method has been called");
        String[][] usernames = new String[][]{
                {"gabriel", "Gabriel", "Deutner", "deu17444@spengergasse.at", Role.GLOBAL_ADMIN},
                {"thomas", "Thomas", "Galler", "gal17470@spengergasse.at", Role.GLOBAL_ADMIN},
                {"matthias", "Matthias", "Walk", "wal16412@spengergasse.at", Role.GLOBAL_ADMIN},
                {"jan", "Jan", "Wadsak", "wad16409@spengergasse.at", Role.GLOBAL_ADMIN},
                {"clemens", "Clemens", "Kern", "ker17529@spengergasse.at", Role.GLOBAL_ADMIN},
                {"hannes", "Hannes", "Foettinger", "hannes.foettinger@spengergasse.at", Role.GLOBAL_ADMIN},
                //Role test users
                {"localuser", "Max", "Mustermann", "localuser@spengergasse.at", Role.LOCAL_USER},
                {"globaluser", "Max", "Mustermann", "globaluser@spengergasse.at", Role.GLOBAL_USER},
                {"localadmin", "Max", "Mustermann", "localadmin@spengergasse.at", Role.LOCAL_ADMIN},
                {"globaladmin", "Max", "Mustermann", "globaladmin@spengergasse.at", Role.GLOBAL_ADMIN}
        };

        CompanyContact hannes = CompanyContact.builder()
                .firstName("Hannes")
                .lastName("Foettinger")
                .address("Brehmstraße 10")
                .emailAddress("Hannes.Foettinger@snt.at")
                .city("Vienna")
                .region("Austria")
                .faxNumber("+1-212-9876543")
                .businessPhone("710-374-6113")
                .mobileNumber("660-730-7925")
                .zipCode("0420")
                .passportNumber("P21320420420231")
                .build();

        CompanyContact christoph = CompanyContact.builder()
                .firstName("Christoph")
                .lastName("Adler")
                .emailAddress("Christoph.Adler@snt.at")
                .address("Brehmstraße 10")
                .city("Vienna")
                .region("Austria")
                .build();

        companyContactRepository.save(hannes);
        companyContactRepository.save(christoph);


        Company[] companies = {
                Company.builder()
                        .companyCode(10)
                        .name("Spengergasse")
                        .address("Spengergasse 20")
                        .taxRegistrationNumber("1020")
                        .isActive(true)
                        .build(),
                Company.builder()
                        .name("S&T AG")
                        .isActive(true)
                        .companyCode(20)
                        .build()
        };


        String[] documentNames = new String[]{
                "Vollversammlung 2000",
                "Vollversammlung 2001",
                "Vollversammlung 2002",
                "Vollversammlung 2003",
                "Vollversammlung 2004",
                "Vollversammlung 2005",
                "Vollversammlung 2006",
                "Vollversammlung 2007",
                "Vollversammlung 2008",
                "Vollversammlung 2009",
                "Vollversammlung 2010",
                "Vollversammlung 2011",
                "Vollversammlung 2012",
                "Vollversammlung 2013",
                "Vollversammlung 2014",
                "Vollversammlung 2015",
                "Vollversammlung 2016",
                "Vollversammlung 2017",
                "Vollversammlung 2018",
                "Vollversammlung 2019",
                "Rechnung Kunde Meier",
                "Rechnung Kunde Huber",
                "Rechnung Kunde Grill",
                "Rechnung Kunde Microsoft",
        };
        String password = "cisco";

        //Inserting Companies
        for (Company a : companies) {
            companyRepository.save(a);
        }

        //Inserting Users
        try {
            for (String[] user : usernames) {
                userRepo.registerNewUserAccount(new User().builder()
                        .username(user[0])
                        .password(password)
                        .firstname(user[1])
                        .lastname(user[2])
                        .emailAddress(user[3])
                        .company(companyRepository.findByCompanyCode(10))
                        .role(roleRepository.findByName(user[4]))
                        .build()
                );
            }
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }


        //Creating an inserting folders

        Folder sntRootFolder = Folder.builder()
                .parentFolder(null)
                .company(companies[1])
                .folderName("S&T AG")
                .build();
        Folder spengergasseRootFolder = Folder.builder()
                .folderName("Spengergasse")
                .company(companies[0])
                .parentFolder(null)
                .build();

        Folder spengergasseBin = Folder.builder()
                .folderName("bin")
                .company(companies[0])
                .parentFolder(spengergasseRootFolder)
                .build();
        Folder sntBin = Folder.builder()
                .folderName("bin")
                .company(companies[1])
                .parentFolder(sntRootFolder)
                .build();


        folderRepository.save(sntRootFolder);
        folderRepository.save(spengergasseRootFolder);
        folderRepository.save(spengergasseBin);
        folderRepository.save(sntBin);

        Share sntToSpengergasse = Share.builder()
                .company(companies[1])
                .shareHoldingTo(companies[0])
                .percentage(100f)
                .isValid(true)
                .build();


        for (int i = 0; i < 20; i++) {
            CompanyFunction companyFunctionHannes = CompanyFunction.builder()
                    .company(companies[1])
                    .function("ITSM Manager")
                    .validFrom(new LocalDate())
                    .companyContact(hannes)
                    .build();

            companyFunctionRepository.save(companyFunctionHannes);
        }

        CompanyFunction companyFunctionChristoph = CompanyFunction.builder()
                .company(companies[1])
                .function("Teamlead ITSM")
                .validFrom(new LocalDate())
                .companyContact(christoph)
                .build();
        companyFunctionRepository.save(companyFunctionChristoph);


        LegalRepresentation legalRepresentationHannes = LegalRepresentation.builder()
                .company(companies[1])
                .function("Proxy")
                .type("Collective")
                .validFrom(new LocalDate())
                .companyContact(hannes)
                .isValid(true)
                .build();

        legalRepresentationRepository.save(legalRepresentationHannes);


        return "Test Data has been inserted!";
    }


    @Transactional
    @GetMapping(path = "/init")
    public String init() throws Exception {
        if (privilegeRepository.count() != 0) return "Database is already initialized!";

        //Initializing DocumentTypes
        DocumentType[] doctypes = {
                DocumentType.builder().type("Agreements").build(),
                DocumentType.builder().type("Annual Shareholder Meetings").build(),
                DocumentType.builder().type("Articles of Incorporation").build(),
                DocumentType.builder().type("By-Laws").build(),
                DocumentType.builder().type("Commercial Register").build(),
                DocumentType.builder().type("Due Dilligence").build(),
                DocumentType.builder().type("Power of Attorney").build(),
                DocumentType.builder().type("Professional Register").build(),
                DocumentType.builder().type("Restructuring").build(),
                DocumentType.builder().type("Share Certificates").build(),
                DocumentType.builder().type("Shareholder Agreements").build(),
                DocumentType.builder().type("Shareholder Resolutions").build(),
                DocumentType.builder().type("Financial Documentation").build(),
                DocumentType.builder().type("IC Contracts").build(),
                DocumentType.builder().type("Other Documents").build()
        };
        for (DocumentType a : doctypes) {
            documentTypeRepository.save(a);
        }


        //Creating TagTypes
        String[][] tagTypes = {
                {TagType.ARCHIVE_ON_DATE, "Archives the Document on the specified day.", "Archive on date"},
                {TagType.REMIND_ON_DATE, "Reminds the target user on the specified day ", "Remind on date"},
                {TagType.SIGNED_BY, "Marks that the file has been signed by someone", "Signed by"}
        };

        //Creating Privileges
        String[] privileges = {
                Privilege.DOCUMENT_READ_ALL,
                Privilege.DOCUMENT_READ_COMPANY,
                Privilege.DOCUMENT_CREATE_ALL,
                Privilege.DOCUMENT_CREATE_COMPANY,
                Privilege.DOCUMENT_MODIFY_ALL,
                Privilege.DOCUMENT_MODIFY_COMPANY,
                Privilege.DOCUMENT_DELETE_ALL,
                Privilege.USER_DELETE_ALL,
                Privilege.USER_CREATE_COMPANY,
                Privilege.USER_CREATE_ALL,
                Privilege.USER_MODIFY_ALL,
                Privilege.USER_MODIFY_COMPANY,
                Privilege.COMPANY_CREATE_ALL,
                Privilege.COMPANY_MODIFY_ALL,
                Privilege.COMPANY_MODIFY_COMPANY,
                Privilege.STOCK_CREATE_ALL,
                Privilege.STOCK_DELETE_ALL,
                Privilege.ROLE_CREATE_ALL,
                Privilege.ROLE_DELETE_ALL,
                Privilege.ROLE_MODIFY_ALL
        };
        //Inserting TagTypes
        for (String[] a : tagTypes) {
            tagTypeRepository.save(
                    TagType.builder()
                            .name(a[0])
                            .description(a[1])
                            .clearName(a[2])
                            .build()
            );
        }


        //Inserting Privileges
        for (String a : privileges) {
            privilegeRepository.save(Privilege.builder().name(a).build());
        }
        //Global admin
        ArrayList<Privilege> globalAdminPrivileges = new ArrayList<>();
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_CREATE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_MODIFY_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_DELETE_ALL).get());

        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.ROLE_CREATE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.ROLE_DELETE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.ROLE_MODIFY_ALL).get());

        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_READ_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_CREATE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_MODIFY_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_DELETE_ALL).get());

        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.STOCK_CREATE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.STOCK_DELETE_ALL).get());

        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.COMPANY_CREATE_ALL).get());
        globalAdminPrivileges.add(privilegeRepository.findById(Privilege.COMPANY_MODIFY_ALL).get());


        //Local Admin
        ArrayList<Privilege> localAdminPrivileges = new ArrayList<>();
        localAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_CREATE_COMPANY).get());
        localAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_DELETE_COMPANY).get());
        localAdminPrivileges.add(privilegeRepository.findById(Privilege.USER_MODIFY_COMPANY).get());

        localAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_READ_COMPANY).get());
        localAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_CREATE_COMPANY).get());
        localAdminPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_MODIFY_COMPANY).get());

        localAdminPrivileges.add(privilegeRepository.findById(Privilege.COMPANY_MODIFY_COMPANY).get());

        //Global User
        ArrayList<Privilege> globalUserPrivileges = new ArrayList<>();
        globalUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_READ_ALL).get());
        globalUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_CREATE_ALL).get());
        globalUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_MODIFY_ALL).get());

        //localUser
        ArrayList<Privilege> localUserPrivileges = new ArrayList<>();
        localUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_CREATE_COMPANY).get());
        localUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_READ_COMPANY).get());
        localUserPrivileges.add(privilegeRepository.findById(Privilege.DOCUMENT_MODIFY_COMPANY).get());


        //Creating Roles
        Role[] roles = new Role[]{
                Role.builder()
                        .name(Role.GLOBAL_ADMIN)
                        .privileges(globalAdminPrivileges)
                        .description("An admin for the whole system. Has all privileges on the highest level.")
                        .build(),
                Role.builder()
                        .name(Role.LOCAL_USER)
                        .privileges(localUserPrivileges)
                        .description("User that has privileges within the company, such as reading or uploading documents..")
                        .build(),
                Role.builder()
                        .name(Role.GLOBAL_USER)
                        .privileges(globalUserPrivileges)
                        .description("User that has privileges extending over company lines, such as reading or uploading documents.")
                        .build(),
                Role.builder()
                        .name(Role.LOCAL_ADMIN)
                        .privileges(localAdminPrivileges)
                        .description("An admin for the company, has admin privileges, but only on company level.")
                        .build()

        };
        //Inserting Roles
        for (Role role : roles) {
            roleRepository.save(role);
        }
        return "Database has been initialized successfully!";
    }

    /**
     * It's not pretty, but it works
     *
     * @throws SQLException
     */
    @GetMapping("logdb")
    public void initTestDB() throws SQLException {
        var con = dataSource.getConnection();
        var stmt = con.createStatement();
        try {
            stmt.executeUpdate("CREATE TABLE logging_event\n" +
                    "  (\n" +
                    "    timestmp         NUMBER(20) NOT NULL,\n" +
                    "    formatted_message  VARCHAR2(4000) NOT NULL,\n" +
                    "    logger_name       VARCHAR(254) NOT NULL,\n" +
                    "    level_string      VARCHAR(254) NOT NULL,\n" +
                    "    thread_name       VARCHAR(254),\n" +
                    "    reference_flag    SMALLINT,\n" +
                    "    arg0              VARCHAR(254),\n" +
                    "    arg1              VARCHAR(254),\n" +
                    "    arg2              VARCHAR(254),\n" +
                    "    arg3              VARCHAR(254),\n" +
                    "    caller_filename   VARCHAR(254) NOT NULL,\n" +
                    "    caller_class      VARCHAR(254) NOT NULL,\n" +
                    "    caller_method     VARCHAR(254) NOT NULL,\n" +
                    "    caller_line       CHAR(4) NOT NULL,\n" +
                    "    event_id          NUMBER(10) PRIMARY KEY\n" +
                    "  );");

        } catch (Exception e) {
            throw e;

        } finally {
            con.close();

        }
    }
}
