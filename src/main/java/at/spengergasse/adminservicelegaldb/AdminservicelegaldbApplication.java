package at.spengergasse.adminservicelegaldb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminservicelegaldbApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminservicelegaldbApplication.class, args);
    }

}
