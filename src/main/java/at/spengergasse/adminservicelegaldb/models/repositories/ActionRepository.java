package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.Action;
import at.spengergasse.adminservicelegaldb.models.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActionRepository extends JpaRepository<Action, Long> {
    @Override
    <S extends Action> S save(S entity);


    List<Action> getAllByTag(Tag tag);

}
