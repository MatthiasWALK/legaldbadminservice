package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.CompanyFunction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyFunctionRepository extends JpaRepository<CompanyFunction, Long> {
}
