package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.Company;
import at.spengergasse.adminservicelegaldb.models.entities.Document;
import at.spengergasse.adminservicelegaldb.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {


    Document findByName(final String name);

    Document findByIdAndAndCompany(Long id, Company company);

    Document findByNameAndCompany(final String name, Company company);

    void deleteById(Long id);

    @Override
    <S extends Document> S save(S s);

    List<Document> findAllByUser(User user);

    List<Document> findALLByName(final String name);

    List<Document> findByNameContainingAllIgnoreCase(final String name);

    List<Document> findAllByIsSigned(final boolean issigned);



    @Override
    void deleteAll();
}
