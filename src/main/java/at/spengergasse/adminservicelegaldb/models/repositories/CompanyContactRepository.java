package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.CompanyContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyContactRepository extends JpaRepository<CompanyContact, Long> {

}
