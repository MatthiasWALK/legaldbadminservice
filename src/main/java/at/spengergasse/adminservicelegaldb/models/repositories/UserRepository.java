package at.spengergasse.adminservicelegaldb.models.repositories;


import at.spengergasse.adminservicelegaldb.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Override
    <S extends User> S save(S s);

    User findByUsername(String username);
    List<User> findByFirstnameContainingIgnoreCaseOrLastnameContainingAllIgnoreCase(String firstname, String lastname);
    List<User> findByFirstnameOrLastnameContainingAllIgnoreCase(String firstname, String lastname);
    List<User> findByFirstnameAndLastnameAllIgnoreCase(String firstname, String lastname);

    @Override
    void deleteById(String s);

    @Override
    void deleteAll();
}
