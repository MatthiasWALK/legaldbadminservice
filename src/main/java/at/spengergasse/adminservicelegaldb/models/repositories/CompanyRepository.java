package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    @Override
    <S extends Company> S save(S s);

    Company findByCompanyCode(Integer companyCode);
    Company findByName(String name);

    @Override
    void deleteAll();
}
