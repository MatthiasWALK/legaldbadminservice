package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.LegalRepresentation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LegalRepresentationRepository extends JpaRepository<LegalRepresentation, Long> {
}
