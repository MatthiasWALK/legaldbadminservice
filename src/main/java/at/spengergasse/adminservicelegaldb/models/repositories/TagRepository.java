package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
    @Override
    <S extends Tag> S save(S s);
    @Override
    void deleteAll();

}
