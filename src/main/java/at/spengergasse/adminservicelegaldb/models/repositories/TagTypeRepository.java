package at.spengergasse.adminservicelegaldb.models.repositories;

import at.spengergasse.adminservicelegaldb.models.entities.TagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagTypeRepository extends JpaRepository<TagType, String> {

    @Override
    <S extends TagType> S save(S s);

    @Override
    Optional<TagType> findById(String s);
}
