package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "step")

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Step {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "st_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "st_w_workflow",referencedColumnName = "w_id", nullable = false)
    private Workflow workflow;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "st_u_user", referencedColumnName = "u_id",nullable = false)
    private User user;

    @NonNull
    @Column(name = "st_isCompleted")
    private Boolean isCompleted;
}
