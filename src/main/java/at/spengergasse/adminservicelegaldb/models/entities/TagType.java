package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;

import javax.persistence.*;
import java.lang.annotation.Documented;

/**
 * @author Jan Wadsak
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "tagtype")

public class TagType {

    //Name
    @Id
    @Column(name = "tt_name", nullable = false)
    private String name;

    //Description
    @Column(name = "tt_description")
    private String description;

    @Column(name = "tt_clearname")
    private String clearName;


    public static final String ARCHIVE_ON_DATE = "ARCHIVE_ON_DATE";
    public static final String REMIND_ON_DATE = "REMIND_ON_DATE";
    //If something is by an individual
    public static final String SIGNED_BY = "SIGNED_BY";
    //Like Contract, or
    public static final String DOCUMENT_TYPE = "DOCUMENT_TYPE";
}
