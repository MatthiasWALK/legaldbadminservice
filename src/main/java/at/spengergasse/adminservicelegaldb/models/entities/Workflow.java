package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "workflow")

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Workflow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "w_id", nullable = false)
    private Long id;

    @Column(name = "w_name", nullable = false)
    private String name;


    @OneToMany(mappedBy ="workflow", cascade = CascadeType.REMOVE)
    private List<Step> steps;

    @ManyToOne
    @JoinColumn(name = "w_d_document",referencedColumnName = "d_id", nullable = false)
    private Document document;
}
