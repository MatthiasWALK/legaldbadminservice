package at.spengergasse.adminservicelegaldb.models.entities;


import at.spengergasse.adminservicelegaldb.models.entities.TagType;
import lombok.*;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Jan Wadsak
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "t_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "t_tt_type", referencedColumnName = "tt_name", nullable = false)
    private TagType type;

    //contents of tag
    @Column(name = "t_content", nullable = false)
    private String content;

    @ManyToOne
    @JoinColumn(name = "t_d_document", referencedColumnName = "d_id", nullable = false)
    private Document document;

    @Column(name = "t_date")
    private LocalDate date;
}
