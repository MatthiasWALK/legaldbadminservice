package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;
import org.joda.time.LocalDate;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "legalRepresentation")
public class LegalRepresentation {

    @Id
    @Column(name="lr_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "cc_id", name = "lr_cc_companyContact")
    private CompanyContact companyContact;

    @ManyToOne
    @JoinColumn(referencedColumnName = "c_id", name = "lr_c_company")
    private Company company;

    @Column(name = "lr_validFrom")
    private LocalDate validFrom;

    @Column(name = "lr_validTo")
    private LocalDate validTo;

    @Column(name = "lr_isValid")
    private Boolean isValid;

    @Column(name = "lr_function")
    private String function;

    @Column(name = "lr_type")
    private String type;

}
