package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "sntPrivilege")
public class Privilege implements GrantedAuthority {
    //Document
    //DOCUMENT_MODIFY includes adding and removing tags from the document, it also includes flagging the document for deletion
    public static final String DOCUMENT_READ_ALL = "DOCUMENT_READ_ALL";
    public static final String DOCUMENT_READ_COMPANY = "DOCUMENT_READ_COMPANY";
    public static final String DOCUMENT_CREATE_ALL = "DOCUMENT_CREATE_ALL";
    public static final String DOCUMENT_CREATE_COMPANY = "DOCUMENT_CREATE_COMPANY";
    public static final String DOCUMENT_MODIFY_ALL = "DOCUMENT_MODIFY_ALL";
    public static final String DOCUMENT_MODIFY_COMPANY = "DOCUMENT_MODIFY_COMPANY";
    public static final String DOCUMENT_DELETE_ALL ="DOCUMENT_DELETE_ALL";

    //User
    public static final String USER_DELETE_ALL = "USER_DELETE_ALL";
    public static final String USER_DELETE_COMPANY = "USER_CREATE_COMPANY";
    public static final String USER_CREATE_ALL = "USER_CREATE_ALL";
    public static final String USER_CREATE_COMPANY = "USER_CREATE_COMPANY";
    //This privilege includes assigning user roles
    public static final String USER_MODIFY_ALL = "USER_MODIFY_ALL";
    //This one does not
    public static final String USER_MODIFY_COMPANY = "USER_MODIFY_COMPANY";

    //Company
    public static final String COMPANY_CREATE_ALL = "COMPANY_CREATE_ALL";
    public static final String COMPANY_MODIFY_ALL = "COMPANY_MODIFY_ALL";
    public static final String COMPANY_MODIFY_COMPANY = "COMPANY_MODIFY_COMPANY";

    //Stock
    public static final String STOCK_CREATE_ALL = "STOCK_CREATE_ALL";
    public static final String STOCK_DELETE_ALL = "STOCK_DELETE_ALL";

    //Role
    public static final String ROLE_CREATE_ALL = "ROLE_CREATE_ALL";
    public static final String ROLE_DELETE_ALL = "ROLE_DELETE_ALL";
    public static final String ROLE_MODIFY_ALL = "ROLE_MODIFY_ALL";

    @Id
    @Column(nullable = false, unique = true, name = "pr_name")
    private String name;

    @Column(name = "pr_description")
    private String description;

    @Override
    public String getAuthority() {
        return name;
    }
}