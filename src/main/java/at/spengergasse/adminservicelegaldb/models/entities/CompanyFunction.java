package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;
import org.joda.time.LocalDate;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "companyFunction")
public class CompanyFunction {
    @Id
    @Column(name="cf_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "cc_id", name = "cf_cc_companyContact")
    private CompanyContact companyContact;

    @ManyToOne
    @JoinColumn(referencedColumnName = "c_id", name = "cc_c_company")
    private Company company;

    @Column(name = "cf_validFrom")
    private LocalDate validFrom;

    @Column(name = "cf_validTo")
    private LocalDate validTo;


    @Column(name = "cf_function")
    private String function;


}
