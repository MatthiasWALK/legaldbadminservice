package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;

import javax.persistence.*;

/**
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "stock")
public class Share {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "s_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "s_c_company", referencedColumnName = "c_id", nullable = false)
    private Company company;

    @ManyToOne
    @JoinColumn(name = "s_c_shareHoldingTo", referencedColumnName = "c_id", nullable = false)
    private Company shareHoldingTo;

    @Column(name = "s_percentage", nullable = false)
    private float percentage;

    @Column(name = "s_isValid")
    private Boolean isValid;
}
