package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "companyContact")
public class CompanyContact {

    @Id
    @Column(name="cc_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Email(message = "Email has to be valid!")
    @NotNull(message = "Email address cannot be null!")
    @Column(name = "cc_emailAddress", unique = true, nullable = false)
    private String emailAddress;

    @Column(name = "cc_passportNumber")
    private String passportNumber;

    @Column(name = "cc_businessPhone")
    private String businessPhone;

    @Column(name = "cc_mobileNumber")
    private String mobileNumber;

    @Column(name = "cc_faxNumber")
    private String faxNumber;

    @Column(name = "cc_address")
    private String address;

    @Column(name = "cc_ZIPCode")
    private String zipCode;

    @Column(name = "cc_city")
    private String city;

    @Column(name = "cc_region")
    private String region;

    @Column(name = "cc_firstname")
    private String firstName;

    @Column(name = "cc_lastname")
    private String lastName;

}
