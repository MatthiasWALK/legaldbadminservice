package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "company")

public class Company {
    @Id
    @Column(name = "c_id", nullable = false)
    private Integer companyCode;

    @Column(name = "c_name", nullable = false, unique = true)
    private String name;

    @Column(name = "c_registeredOffice")
    private String registeredOffice;

    @Column(name = "c_ZIPCode")
    private String zipCode;

    @Column(name = "c_companyRegistrationNumber")
    private String companyRegistrationNumber;

    @Column(name = "c_isActive", nullable = false)
    private boolean isActive;

    @Column(name = "c_address")
    private String address;

    @Column(name = "c_VATRegistrationNumber")
    private String vatRegistrationNumber;

    @Column(name = "c_taxRegistrationNumber")
    private String taxRegistrationNumber;

    @Column(name = "c_information")
    private String information;
}
