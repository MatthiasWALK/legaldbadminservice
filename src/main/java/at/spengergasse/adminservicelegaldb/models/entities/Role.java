package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "sntrole")
public class Role {
    @Id
    @Column(name = "r_name", nullable = false)
    private String name;

    @Column(name = "r_description")
    private String description;


    public static final String GLOBAL_ADMIN = "GLOBAL_ADMIN";
    public static final String LOCAL_ADMIN = "LOCAL_ADMIN";

    public static final String LOCAL_USER = "LOCAL_USER";
    public static final String GLOBAL_USER = "GLOBAL_USER";

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    private List<Privilege> privileges;
}