package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;
import org.joda.time.LocalDate;

import javax.persistence.*;

/**
 * @author Jan Wadsak
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "action")
public class Action {
    //Identity
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "a_id")
    private Long Id;

    //Tag Type
    @ManyToOne
    @JoinColumn(name = "a_tt_type")
    private TagType type;

    //Datum
    @Column(name = "a_date")
    private LocalDate date;

    //Value
    @Column(name = "a_value")
    private String action;

    //Tag
    @ManyToOne
    @JoinColumn(name = "a_t_tag")
    private Tag tag;
}
