package at.spengergasse.adminservicelegaldb.models.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "document_Type")
public class DocumentType {
    @Id
    @Column(name = "dt_type", nullable = false)
    private String type;

    @Column(name = "dt_description")
    private String description;
}
