package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "folder")
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "f_id", nullable = false)
    private Long id;

    @Column(name = "f_name", nullable = false)
    private String folderName;

    @OneToMany(mappedBy = "parentFolder", cascade = CascadeType.ALL)
    private List<Folder> folders;

    @OneToMany(mappedBy = "folder", cascade = CascadeType.ALL)
    private List<Document> documents;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "f_id", name = "f_f_parentFolder")
    private Folder parentFolder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "c_id", name = "f_c_company")
    private Company company;

}
