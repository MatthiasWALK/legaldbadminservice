package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.util.List;

/**
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "document")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "d_id", nullable = false)
    private Long id;

    @Column(name = "d_name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "d_u_user", referencedColumnName = "u_id")
    private User user;

    @Lob
    @Column(name = "d_record")
    private byte[] record;

    @Column(name = "d_mimeType")
    private String mimeType;

    @Column(name = "d_fileEnding", nullable = false)
    private String fileEnding;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "d_c_company", referencedColumnName = "c_id")
    private Company company;

    @Column(name = "d_isSigned")
    private Boolean isSigned;

    @Column(name = "d_signedOn")
    private LocalDate signedOn;

    @Column(name = "d_isValidUntil")
    private LocalDate isValidUntil;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "d_d_isReplacedBy", referencedColumnName = "d_id")
    private Document isReplacedBy;

    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL)
    private List<Tag> tags;

    @Column(name = "d_isDeleted", nullable = false)
    private Boolean isDeleted;

    @Column(name = "d_isArchived", nullable = false)
    private Boolean isArchived;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "f_id", name = "d_f_folder")
    private Folder folder;

    @Column(name = "d_uploadedOn", nullable = false)
    private LocalDate uploadedOn;

    @Column(name = "d_fileSizeInKB")
    private float fileSizeInKB;

    @Column(name = "d_parties")
    private String[] parties;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "dt_type", name = "d_dt_type")
    private DocumentType type;

}
