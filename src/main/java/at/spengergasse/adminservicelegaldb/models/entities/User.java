package at.spengergasse.adminservicelegaldb.models.entities;


import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Matthias Walk
 */

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "sntUser")
public class User implements UserDetails {

    @Id
    @Column(name="u_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Username cannot be null!")
    @Column(name = "u_username", unique = true, nullable = false)
    private String username;

    @Email(message = "Email has to be valid!")
    @NotNull(message = "Email address cannot be null!")
    @Column(name = "u_emailAddress", unique = true, nullable = false)
    private String emailAddress;


    @NotNull(message = "Passoword cannot be null!")
    @Column(name = "u_password", nullable = false)
    private String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_c_company", referencedColumnName = "c_id", nullable = false)
    private Company company;


    @ManyToOne
    @JoinColumn(name = "u_r_role", referencedColumnName = "r_name")
    private Role role;

    @Column(name = "u_firstname")
    private String firstname;

    @Column(name = "u_lastname")
    private String lastname;

    @Column(name = "u_customScaling")
    private Float customScaling;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.addAll(role.getPrivileges());
        return grantedAuthorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
