package at.spengergasse.adminservicelegaldb;

import at.spengergasse.adminservicelegaldb.models.entities.User;
import at.spengergasse.adminservicelegaldb.models.repositories.PrivilegeRepository;
import at.spengergasse.adminservicelegaldb.models.repositories.UserRepository;
import jdk.jshell.spi.ExecutionControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Matthias Walk
 */

@Service
public class UserService implements UserDetailsService {

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String s) {
        User user = userRepository.findByUsername(s);
        if (user == null) throw new UsernameNotFoundException("Username " + s + " could not be found!");
        return user;
    }

    @PreAuthorize(value = "canDelete(#userName)")
    public boolean deleteUser(String userName) {
        if (userName == null) return false;
        if (null == userRepository.findByUsername(userName)) return false;
        userRepository.deleteById(userName);
        return true;
    }

    /**
     * Not implemented
     * @param permissionToAdd
     * @param targetUserName
     * @return
     */
    @PreAuthorize( "#canModifyUser(#permissionToAdd, #targetUserName)")
    public boolean addPermissionToUser(String permissionToAdd, String targetUserName) throws ExecutionControl.NotImplementedException {
        if (permissionToAdd == null || privilegeRepository.findById(permissionToAdd) == null)
            throw new IllegalArgumentException("Cannot add permission to user:" + targetUserName + ". Privilege is null or does not exist!");
        if (targetUserName==null ||  userRepository.findByUsername(targetUserName)==null)
            throw new IllegalArgumentException("Cannot add permission to user: username is null or doesnt exist!");

        throw new ExecutionControl.NotImplementedException("");


    }

    /**
     * Registers new user
     * The acting user has to have the privileges
     * @param user
     * @return
     * @throws Exception
     */
    @PreAuthorize(value = "canCreateUser(#user)")
    public User registerNewUserAccount(User user) throws Exception {
        if (user.getUsername() == null || user.getUsername().equals(""))
            throw new IllegalArgumentException("Cannot register new user: username is null or empty! ");
        if (userRepository.findByUsername(user.getUsername()) != null)
            throw new UsernameNotFoundException("Cannot register new user: username already exists!");
        if (user.getPassword() == null || user.getPassword().equals(""))
            throw new Exception("Cannot register new user: password is null or empty!");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }


}
